@extends('layouts.app')
@section('title') Add Post @endsection
@section('content')
    <h1 class="mt-4">Create New Post</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Post Info</li>
    </ol>
    <div>
        <a class="btn  btn-primary btn-sm" href="{{url('posts')}}">Post List</a> <br><br>
    </div>
    <form method="post" action="{{url('posts/save')}}">
        @csrf
        <div class="form-floating mb-3">
            <label for="title">Title</label>
            <input class="form-control" name="title" type="text" placeholder="Title" />
            <span class="text-center text-danger">{{$errors->has('title') ? $errors->first('title') : ''}}</span>
        </div>
        <div class="form-floating mb-3">
            <label for="body">Content</label>
            <textarea class="form-control" name="body" placeholder="Content" rows="3"></textarea>
            <span class="text-center text-danger">{{$errors->has('body') ? $errors->first('body') : ''}}</span>
        </div>
        <div class="d-flex align-items-center justify-content-between mt-4 mb-0">
            <button type="submit" class="btn btn-info">Save</button>
        </div>
    </form>
@stop

