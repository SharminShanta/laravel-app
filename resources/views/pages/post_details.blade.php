@extends('layouts.app')
@section('title') Posts Details @endsection
@section('content')
    <h1 class="mt-4">Post Details</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Post Info</li>
    </ol>
    <div>
        <a class="btn  btn-primary btn-sm" href="{{url('posts')}}">Post List</a> <br><br>
    </div>
    <form method="post" action="{{url('posts/update')}}">
        @csrf
        <div class="form-floating mb-3">
            <label for="title">Title</label>
            <input class="form-control" value="{{$postDetails->id}}" name="id" type="hidden" />
            <input class="form-control" value="{{$postDetails->title}}" name="title" type="text" placeholder="Title" />
            <span class="text-center text-danger">{{$errors->has('title') ? $errors->first('title') : ''}}</span>
        </div>
        <div class="form-floating mb-3">
            <label for="body">Content</label>
            <textarea class="form-control" name="body" placeholder="Content" rows="3">
                {{$postDetails->body}}
            </textarea>
            <span class="text-center text-danger">{{$errors->has('body') ? $errors->first('body') : ''}}</span>
        </div>
        <div class="d-flex align-items-center justify-content-between mt-4 mb-0">
            <button type="submit" class="btn btn-info">Update</button>
        </div>
    </form>
@stop

