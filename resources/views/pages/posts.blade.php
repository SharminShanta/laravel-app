@extends('layouts.app')
@section('title') Posts @endsection
@section('content')
    <h1 class="mt-4">Post List</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">All Posts</li>
    </ol>
    <a class="btn  btn-primary btn-sm" href="{{url('posts/add')}}">Add New</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Content</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        @if(count($posts) > 0)
        <tbody>
        @php($i = 0)
            @foreach($posts as $post)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$post->title}}</td>
                    <td>{{$post->body}}</td>
                    <td>
                        <a class="text-decoration-none" title="Edit this Post?" href="{{url('posts/edit', [$post->id])}}">Edit</a>
                        <a class="text-danger text-decoration-none" title="Delete this Post ?" onclick="event.preventDefault();
                            var check = confirm('Are you sure to delete this ?');
                            if(check){
                            document.getElementById('deletePost'+'{{$post->id}}').submit();
                            }
                            ">Delete</a>
                        <form action="{{url('posts/delete', [$post->id])}}" id="deletePost{{$post->id}}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{$post->id}}">
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
        @else
            <span class="text-danger">No Data Found</span>
        @endif
    </table>
@stop


