<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Resource Route
//Route::resource('posts', \App\Http\Controllers\PostController::class);

//Redirect Routes - defining a route that redirects to another URI
Route::redirect('/here', '/posts', 301);

//View Routes - defining a route return a view
Route::view('/welcome', 'welcome', ['name' => 'Taylor']);

//Required Parameters - capture segments of the URI within route
Route::get('/user/{id}', function ($id) {
    return 'User '.$id;
});

//Optional Parameters - specify a route parameter that may not always
Route::get('/user/{name?}', function ($name = null) {
    return $name;
});

//if parameter is not specified, returns "Jhon"
Route::get('/user/{name?}', function ($name = 'John') {
    return $name;
});

//Regular Expression Constraints - constrain the format of route parameters
//Route::get('/user/{id}/{name}', function ($id, $name) {
//    return "The ID is ". $id . ", The name is ". $name;
//})->where(['id' => '[0-9]+', 'name' => '[a-z]+']);

Route::get('/user/{id}/{name}', function ($id, $name) {
    return "The ID is ". $id . ", The name is ". $name;
})->whereNumber('id')->whereAlpha('name');

//Section 4, 5, 6, 7: Laravel Fundamental: Routes, Controller,Views - Dashboard, contact View with Layout
Route::get('/', [\App\Http\Controllers\DashboardController::class, 'dashboardView']);
Route::get('/dashboard', [\App\Http\Controllers\DashboardController::class, 'dashboardView']);

//Passing Data in Views
Route::get('posts/{id}/{name}/{password}', [\App\Http\Controllers\PostController::class, 'dataPassing']);

//Section 9: Laravel Fundamental: Database - RAW SQL Queries - Inserting, Reading, Updating and Deleting Data into Database
//Inserting Data
Route::get('/insert', function () {
    \Illuminate\Support\Facades\DB::insert('insert into posts(title, body) values(?, ?)',
        ['PHP with Laravel', 'Laravel is the framework of PHP']);
});

//Updating Data
Route::get('/update', function () {
    $updatedPost = \Illuminate\Support\Facades\DB::update('update posts set title = "Become a master in Laravel" where id=?', [1]);
    return $updatedPost;
});

//Deleting Data
Route::get('/delete', function () {
    $deletedPost = \Illuminate\Support\Facades\DB::delete('delete from posts where id=?', [1]);
    return $deletedPost;
});

//Section 10: Laravel Fundamental: Database - Eloquent/ORM - Reading Data, Reading and Finding the Constraints
Route::group(['prefix' => 'posts'], function () {
    Route::get('/', [\App\Http\Controllers\PostController::class, 'index']); //Posts list View
    Route::get('/add', [\App\Http\Controllers\PostController::class, 'create']); //Post Add View
    Route::post('/save', [\App\Http\Controllers\PostController::class, 'store']); //Post Store Process
    Route::get('/edit/{id}', [\App\Http\Controllers\PostController::class, 'edit']); //Post Edit View
    Route::post('/update', [\App\Http\Controllers\PostController::class, 'update']); //{Post Update Process
    Route::post('/delete/{id}', [\App\Http\Controllers\PostController::class, 'destroy']); //Post Delete Process
});


