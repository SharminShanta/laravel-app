<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
Use Illuminate\Database\Eloquent\SoftDeletes;
use PhpParser\Node\Expr\Cast\Int_;
use Ramsey\Uuid\Type\Integer;

class Post extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = "posts";

    /**
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * @var string[]
     */
    protected $fillable = [
        'title', 'body', 'is_admin'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    //protected $dates = 'deleted_at';

    /**
     * Retrieve a listing of the posts.
     *
     * @return Post[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAllPosts()
    {
        return self::all();
    }

    /**
     * Get details of specific record of the posts.
     *
     * @param int $id
     * @return false
     */
    public static function postDetails(int $id)
    {
        $postDetails = self::where('id', $id)->first();
        if ($postDetails != null) {
            return $postDetails;
        }
        return false;
    }

    /**
     * Add a record into posts.
     *
     * @param array $postData
     * @return bool
     */
    public function createPost(array $postData)
    {
        $this->title = $postData['title'];
        $this->body = $postData['body'];
        $this->is_admin = 1;
        $this->created_at = date("Y-m-d h:i:s");

        if ($this->save() === true) {
            return true;
        }
        return false;
    }

    /**
     * Update specific records of the posts.
     *
     * @param $postData
     * @return bool
     */
    public static function updatePost($postData)
    {
        $post = self::find($postData['id']);
        if ($post !== null) {
            $data['title'] = $postData['title'];
            $data['body'] = $postData['body'];
            $data['is_admin'] = 1;
            $data['updated_at'] = date("Y-m-d h:i:s");
            $updated = $post->update($data);

            if ($updated === true) {
                return true;
            }
            return false;
        }

        return false;
    }

    /**
     * Delete specific records of the posts.
     *
     * @param int $id
     * @return bool
     */
    public static function deletePost(int $id)
    {
        $post = self::find($id);
        if ($post !== null) {
            $deleted = $post->delete();

            if ($deleted === true) {
                return true;
            }
            return false;
        }
        return false;
    }
}
