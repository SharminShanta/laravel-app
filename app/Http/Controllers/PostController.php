<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

/**
 * Class PostController
 * @package App\Http\Controllers
 */
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $posts = Post::getAllPosts();
        return view('pages.posts', [
            'posts' => $posts
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('pages.post_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = $this->postAddValidation($request->all());
        if ($validator->fails()) {
            Session::flash('error', 'Invalid Post Data!');
            return redirect()->back()
                ->withInput($request->all())
                ->withErrors($validator->messages());
        }

        try {
            $postModel = New Post();
            $createPost = $postModel->createPost($request->all());
            if ($createPost === true) {
                Session::flash('success', 'Post has created successfully.');
                Log::info('New post has been created.');
                return redirect('posts');
            } else {
                Session::flash('error', 'Invalid Post Data!');
                Log::warning('invalid Post Data');
                return redirect('posts/add');
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            Log::debug($exception->getTraceAsString());
            return redirect('posts/add');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(int $id)
    {
        $postDetails = Post::postDetails($id);
        return view('pages.post_details', [
            'postDetails' => $postDetails,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $validator = $this->postAddValidation($request->all());
        if ($validator->fails()) {
            Session::flash('error', 'Invalid Post Data!');
            return redirect()->back()
                ->withInput($request->all())
                ->withErrors($validator->messages());
        }

        try {
            $updatePost = Post::updatePost($request->all());
            if ($updatePost === true) {
                Session::flash('success', 'Post has updated successfully.');
                Log::info('Post Update Success.');
                return redirect('posts');
            } else {
                Session::flash('error', 'User hasn\'t updated yet.');
                Log::info('User Update Failed.');
                return redirect('posts/edit/');
            }
        } catch (\Exception $exception) {
            Session::flash('error', 'Something went wrong. Try later again.');
            Log::error($exception->getMessage());
            Log::debug($exception->getTraceAsString());
            return redirect('posts/edit/'. $request->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(int $id)
    {
        try {
            $deletePost = Post::deletePost($id);
            if ($deletePost === true) {
                Session::flash('success', 'Post has deleted successfully.');
                return redirect('posts');
            } else {
                Session::flash('error', 'Post hasn\'t deleted yet. Try later again.');
                return redirect('posts');
            }
        } catch (\Exception $exception) {
            Session::flash('error', 'Something went wrong. Try later again.');
            Log::error($exception->getMessage());
            Log::debug($exception->getTraceAsString());
            return redirect('posts');
        }

    }

    /**
     * Passing data into view
     *
     * @param int $id
     * @param string $name
     * @param string $password
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function dataPassing(int $id, string $name, string $password)
    {
        /*return view('pages.index')
            ->with('id', $id);*/

        /*return view('pages.index', [
            'id' => $id
        ]);*/

        //return view('pages.index', compact(['id', 'name', 'password']));

        return view('pages.post', compact(['id', 'name', 'password']));
    }

    /**
     * Data validation of user input data
     *
     * @param array $data
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Validation\Factory|\Illuminate\Contracts\Validation\Validator
     */
    protected function postAddValidation(array $data){
        $rules = [
            'title'        =>  'required',
            'body'         =>  'required',
        ];

        $messages = [
            'title.required' => 'Post title is required.',
            'body.required' => 'Post body is required.',
        ];

        return validator($data, $rules, $messages);
    }
}
